
public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time(10, 30, 45);
        Time time2 = new Time(12, 15, 20);

        System.out.println("Time 1: " + time1.toString());
        System.out.println("Time 2: " + time2.toString());

        // Tăng time1 lên 1 giây
        time1.nextSecond();

        // giảm time2 đi 1 giây
        time2.previousSecond();

        // Print updated time objects
        System.out.println(" Time1 tăng 1 giây: " + time1.toString());
        System.out.println("Time2 giảm 1 giây: " + time2.toString());
    }

}